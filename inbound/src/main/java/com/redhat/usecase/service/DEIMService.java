package com.redhat.usecase.service;

import com.customer.app.Person;

import javax.ws.rs.core.Response;

public interface DEIMService {
  public Response addPerson(Person person);
}
