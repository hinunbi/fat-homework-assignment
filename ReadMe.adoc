:sectnums:
:toc:
# Advanced Agile Integration Trainining Homework

## Prerequsite

We use the below ports on the test desktop/server. so these ports are available on the test desktop/server.
|===
|project | web port | service port
|mq-service | 8081 | 61616
|inbound | 8082 | 9098
|xlate | 8083 |
|outboud | 8084 |
| integration-test-server | 8085 |8085
|===

## Build & Test steps

. Git clone
+
[source, bash]
----
git clone https://gitlab.com/hinunbi/fat-homework-assignment.git
cd fat-homework-assignment
----

. Install homework assignment
+
[source, bash]
----
cd fat-homework-assignment
mvn install
----

. Run mq-service (ActiveMQ broker)
+
If you run the AMQ 6.x on your desktop already.
This step shoud be skipped.
and AMQ 6.x must accept the below user.
+
.Table AQM 6.x user info
[%header, cols="e,^"]
|===
| user info | value
| user |admin
| password |admin
|===
+
[source, bash]
----
cd fat-homework-assignment/mq-service
mvn spring-boot:run
----

. run integration-test-server
+
[source, bash]
----
cd fat-homework-assignment/integration-test-server
mvn spring-boot:run
----

. run inbound
+
[source, bash]
----
cd fat-homework-assignment/inbound
mvn spring-boot:run
----

. run xlate
+
[source, bash]
----
cd fat-homework-assignment/xlate
mvn spring-boot:run
----

. run outbound
+
[source, bash]
----
cd fat-homework-assignment/outbound
mvn spring-boot:run
----

. run test request
+
[source, bash]
----
curl -X POST \
  http://localhost:9098/cxf/demos/match \
  -H 'Content-Type: application/xml' \
  -d '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Person xmlns="http://www.app.customer.com">
  <age>30</age>
  <legalname>
    <given>First</given>
    <family>Last</family>
  </legalname>
  <fathername>Dad</fathername>
  <mothername>Mom</mothername>
  <gender>
    <code>Male</code>
  </gender>
</Person>'
----

. check the logs
+
[source, bash]
----
cd fat-homework-assignment/integration-test-server
cd fat-homework-assignment/outbound
----

